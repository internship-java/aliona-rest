package md.codefactory.service;

import md.codefactory.models.Manga;
import md.codefactory.models.MangaResult;
import md.codefactory.utils.JsonUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.anyOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class MangaServiceUnitTest {

    @Autowired
    private MangaService mangaService;

    @MockBean
    private RestTemplate template;

    @Test
    public void getMangasByTitle() throws IOException {

        // Parsing mock file
        MangaResult mRs = JsonUtils.jsonFile2Object("ken.json", MangaResult.class);

        // Mocking remote service
        when(template.getForEntity(Mockito.any(String.class),Mockito.any(Class.class)))
                .thenReturn(new ResponseEntity(mRs, HttpStatus.OK));

        // I search for goku but system will use mocked response containing only ken, so I can check that mock is used.
        List<Manga> mangasByTitle = mangaService.getMangasByTitle("goku");

        assertThat(mangasByTitle).isNotNull()
                .isNotEmpty()
                .allMatch(p -> p.getTitle()
                        .toLowerCase()
                        .contains("ken"));
    }
}