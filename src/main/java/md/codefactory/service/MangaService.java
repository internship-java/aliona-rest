package md.codefactory.service;

import md.codefactory.models.Manga;
import md.codefactory.models.MangaResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class MangaService {
    Logger logger = LoggerFactory.getLogger(MangaService.class);
    //private static final String MANGA_SEARCH_URL="https://api.jikan.moe/search/manga/";
  //  private static final String MANGA_SEARCH_URL="https://api.jikan.moe/v3";
    private static final String MANGA_SEARCH_URL="https://api.jikan.moe/v3/manga/1/characters";

    @Autowired
    RestTemplate restTemplate;

    public List<Manga> getMangasByTitle(String title) {
        return restTemplate.getForEntity(MANGA_SEARCH_URL+title, MangaResult.class).getBody().getResult();
    }
}
